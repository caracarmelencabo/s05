<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Confirmation</title>
</head>
<body>

	<%
		String appDiscovery = session.getAttribute("appDiscovery").toString();
		
		if(appDiscovery.equals("friends")){
			appDiscovery="friends";
		}else if (appDiscovery.equals("socialmedia")){
			appDiscovery="social media";
		}else{
			appDiscovery="others";
		} 
		
		String birthdate = session.getAttribute("birthdate").toString();
	%>
	
	<h1>Registration confirmation</h1>
	<p>First Name: <%=session.getAttribute("firstName") %></p>
	<p>Last Name: <%=session.getAttribute("lastName") %></p>
	<p>Phone: <%=session.getAttribute("phone") %></p>
	<p>Email: <%=session.getAttribute("email") %></p>
	<p>App Discovery: <%=appDiscovery %></p>
	<p>Date of Birth: <%=birthdate %></p>
	<p>User Type: <%=session.getAttribute("userType") %></p>
	<p>Description: <%=session.getAttribute("profileDescription") %></p>
	
	<form action="login" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>
	

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>

	<h1>Welcome <%=session.getAttribute("fullName") %>!</h1>
	
	<% 
		String userType = session.getAttribute("userType").toString();
		
		if(userType.equals("applicant")){
			out.println("Welcome applicant. You may now start looking for your career opportunity.");
		}else if(userType.equals("employer")){
			out.println("Welcome employer. You may now start browsing applicant profiles.");
		}
	%>
	

</body>
</html>